package main;

import java.util.Stack;
import java.util.Iterator;
import java.util.Scanner;

public class Desafio {
	
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Stack<Integer> pilhaNumeros = new Stack<Integer>();
		Integer numero;
		
		System.out.println("LISTA DE N�MEROS");
		do {
			System.out.println("Informe um n�mero: ");
			numero = scanner.nextInt();
			pilhaNumeros.add(numero);
		} while (numero >= 0);
		
		System.out.println("Os n�meros na pilha s�o: ");
		Iterator<Integer> it = pilhaNumeros.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	public static Integer primo(Stack<Double> pilha) {
		Integer aux = 0;
		for(int i = 0; i < pilha.size()-1; i ++) {
			if(pilha.elementAt(i) % 2 != 0 && pilha.elementAt(i + 1) % 2 != 0) {
				aux++;
			}
		}
		return aux;
	}
}
